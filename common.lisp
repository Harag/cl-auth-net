(in-package :cl-auth-net)

(defparameter *sandbox-mode* t)

(defparameter *merchant* "5KP3u95bQpv")
(defparameter *transaction-key* "346HZ32z3fP4hTG2")
(defparameter *url* "https://apitest.authorize.net/xml/v1/request.api")

(defun toggle-sandbox-mode ()
  (cond (*sandbox-mode*
	  (setf *sandbox-mode* nil)
	  (setf *url* "https://api.authorize.net/xml/v1/request.api"))
	((not  *sandbox-mode*)
	  (setf *sandbox-mode* t)
	 (setf *url* "https://apitest.authorize.net/xml/v1/request.api")))
  *sandbox-mode*)

(defun api-json (api-name &key (merchant-name *merchant*) (transaction-key *transaction-key*)
			    parameter-plist)
  (let ((auth (list :merchant-authentication
		    (list :name merchant-name
			  :transaction-key transaction-key))))
    
    (list api-name (if parameter-plist
		       (append auth parameter-plist)
		       auth))))

(defun encode-plist (plist)
  (json:with-object
   ()
   (loop for (a b) on plist by #'cddr 
      do (if (listp a)
	     (encode-plist a)
	     (json:as-object-member (a)
				    (if (listp b)
					(encode-plist b )
					(json:encode-json b)))))))

(defun encode-json (object)
  (with-output-to-string (json:*json-output*)
    (encode-plist object)
    json:*json-output*))

(eval-when (:compile-toplevel :load-toplevel :execute)
 ;; See https://en.wikipedia.org/wiki/Byte_order_mark
 (defconstant +bom+     #xfeff)
 (defconstant +bad-bom+ #xfffe))

(defun decode-json (octets)
  (let ((json:*identifier-name-to-key* #'nstring-downcase)
        (json:*json-identifier-name-to-lisp* #'json:simplified-camel-case-to-lisp)
	(string-value (if (stringp octets)
			  octets
			  (babel:octets-to-string octets))))

    ;; Removing the BOM from front of string
    (setf string-value
          (case (char-code (aref string-value 0))
            (#.+bom+  (subseq string-value 1))
            (#.+bad-bom+
             ;; TODO: we could re-process the octets taking into account the byte order.  If we get this error, we can add an issue to do that.
             (error "UTF-8 decoding error: we get an invalid BOM. ~S" string-value))
            (otherwise string-value)))
       
    (json:decode-json-from-string string-value)))


(defun alist-plist (alist)
  (let (plist)
    (dolist (pair alist)
      (cond
	((atom (car pair))
         
	 (push (intern (string-upcase (car pair)) :KEYWORD) plist)
	 (push (if (listp (cdr pair))
		   (if (> (length (cdr pair) ) 1)		       
		       (alist-plist (cdr pair))
		       (if (listp (car (cdr pair)))
			   (alist-plist (cdr pair))
			   (first (cdr pair))))
		   (cdr pair))
		   plist))
	((listp pair)
	 (push  (alist-plist pair )
		plist))))
    (nreverse plist)))

(defun api-call (content-plist &key (url *url*))
  (let ((result (decode-json
		 (drakma:http-request url
				      :method :post
				      :content-type "application/json" 
				      :decode-content t
				      :content
				      (encode-json content-plist))))
	(final))
    ;;(break "~S~%~S" content-plist result)
    (setf final (alist-plist result))
    ;;(break "~S" final)
    final))


(defun error-p (result)
  (when (and (getf result :messages)
	       (string-equal (getf (getf result :messages) :result-code) "Error"))
      (getf (getf result :messages) :message)))

(defun ok-p (result)
  (when (and (getf result :messages)
	     (string-equal (getf (getf result :messages) :result-code) "Ok"))
	(getf (getf result :messages) :message)))

(defun message-code-p (result code)
  (when (and
	   (getf result :messages)
	   (getf (getf result :messages) :message)
	   (string-equal (getf (first (getf (getf result :messages) :message)) :code)
			 code))
    (getf (getf result :messages) :message)))

(defun success-p (result)
  (message-code-p result "I00001"))
