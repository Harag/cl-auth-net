(defsystem "cl-auth-net"
  :description "Implements select few of authorize.net's api's in Common Lisp."
  :version "2019.11.10"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on ("drakma" "cl-json")
  :components ((:file "packages")
	       (:file "common" :depends-on ("packages"))
	       (:file "customer-profile" :depends-on ("common"))
	       (:file "recurring-billing" :depends-on ("common"))
	       (:file "payment-transactions" :depends-on ("common"))
	       (:file "authorize" :depends-on ("common"
					       "customer-profile"
					       "recurring-billing"
					       "payment-transactions"))
	       
	       ))

