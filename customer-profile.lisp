(in-package :cl-auth-net)

(defun create-customer-profile-request (merchant-customer-id description email
					card-number expiration-date				
					&key (customer-type "individual")
                                        bill-to  
				       (merchant-name *merchant*)
					  (transaction-key *transaction-key*))

  (let ((payment-profiles (if bill-to
			      (list :customer-type customer-type
				    :bill-to bill-to
				    :payment
				    (list :credit-card
					  (list :card-number card-number
						:expiration-date expiration-date)))
			      (list :customer-type customer-type			     
				    :payment
				    (list :credit-card
					  (list :card-number card-number
						:expiration-date expiration-date))))))
    (api-json "createCustomerProfileRequest"
	      :merchant-name merchant-name
	      :transaction-key transaction-key
	      :parameter-plist
	      (list :profile 
		    (list :merchant-customer-id merchant-customer-id
			  :description description
			  :email email
			  :payment-profiles payment-profiles)
		    :validation-mode "testMode"))))


(defun create-customer-credit-card-profile-request (email description merchant-customer-id
						    customer-type card-number expiration-date
						    &key 
						      (merchant-name *merchant*)
						      (transaction-key *transaction-key*))
  (api-json "createCustomerProfileRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :profile (list :merchant-customer-id merchant-customer-id
				 :description description
				 :email email
				 :payment-profiles
				 (list
				  :customer-type customer-type
				  :payment (list :credit-card
						 (list :card-number card-number
						       :expiration-date expiration-date)))))))

(defun create-customer-bank-account-profile-request (email description merchant-customer-id
						     customer-type account-type account-number
						     routing-number name-on-account
						     &key 
						       (merchant-name *merchant*)
						       (transaction-key *transaction-key*))
  (api-json "createCustomerProfileRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :profile (list :merchant-customer-id merchant-customer-id
				 :description description
				 :email email
				 :payment-profiles
				 (list
				  :customer-type customer-type
				  :payment (list :bank-account
						 (list :account-type account-type
						       :account-number account-number
						       :routing-number routing-number
						       :name-on-account name-on-account)))))))


(defun get-customer-profile-request (customer-profile-id
				     &key (include-issuer-info-p nil)
				       (merchant-name *merchant*)
				       (transaction-key *transaction-key*))
  (api-json "getCustomerProfileRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :customer-profile-id customer-profile-id
		  :include-issuer-info (if include-issuer-info-p
					   "true"
					   "false"))))

(defun get-customer-profile-ids-request (&key (merchant-name *merchant*)
					   (transaction-key *transaction-key*))
  (api-json "getCustomerProfileIdsRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key))



(defun update-customer-profile-request (customer-profile-id merchant-customer-id
					description email 
					&key 
					  (merchant-name *merchant*)
					  (transaction-key *transaction-key*))
  (api-json "updateCustomerProfileRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :profile (list
			    :customer-profile-id customer-profile-id
			    :merchant-customer-id merchant-customer-id
			    :description description
			    :email email))))

(defun delete-customer-profile-request (customer-profile-id
					&key 
					  (merchant-name *merchant*)
					  (transaction-key *transaction-key*))
  (api-json "deleteCustomerProfileRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :customer-profile-id customer-profile-id)))



(defun create-customer-payment-profile-request (customer-profile-id card-number expiration-date	
						&key 
                                        bill-to (default-payment-profile-p nil) 
				       (merchant-name *merchant*)
					  (transaction-key *transaction-key*))

  (let ((payment-profiles (if bill-to
			      (list 
				    :bill-to bill-to
				    :payment
				    (list :credit-card
					  (list :card-number card-number
						:expiration-date expiration-date)))
			      (list 			     
				    :payment
				    (list :credit-card
					  (list :card-number card-number
						:expiration-date expiration-date))))))
    (api-json "createCustomerPaymentProfileRequest"
	      :merchant-name merchant-name
	      :transaction-key transaction-key
	      :parameter-plist
	      (list :customer-profile-id customer-profile-id     
		    :payment-profiles payment-profiles
		    :default-payment-profile (if default-payment-profile-p
						 "true"
						 "false")
		    :validation-mode "testMode"))))


(defun get-customer-payment-profile-request (customer-profile-id customer-payment-profile-id
					     &key 
					       (merchant-name *merchant*)
					       (transaction-key *transaction-key*))
  (api-json "getCustomerPaymentProfileRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :customer-profile-id customer-profile-id
		  :customer-payment-profile-id customer-payment-profile-id)))


(defun get-customer-payment-profile-list-request (month
						  &key
						    (paging-limit 10)
						    (paging-offset 1)
						    (search-type "cardsExpiringInMonth")
					       (merchant-name *merchant*)
					       (transaction-key *transaction-key*))
  (api-json "getCustomerPaymentProfileListRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist	    
	    (list :search-type search-type
		  :month month
		  :sorting (list :order-by "id"
				 :order-descending "false")
		  :paging (list :limit paging-limit
				:offset paging-offset)
	     )))



(defun validate-customer-payment-profile-request (customer-profile-id customer-payment-profile-id
					     &key 
					       (merchant-name *merchant*)
					       (transaction-key *transaction-key*))
  (api-json "validateCustomerPaymentProfileRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :customer-profile-id customer-profile-id
		  :customer-payment-profile-id customer-payment-profile-id
		  :validation-mode "testMode")))

(defun update-customer-payment-profile-request (customer-profile-id customer-payment-profile-id
						card-number expiration-date	
						&key 
						  bill-to (default-payment-profile-p nil) 
						  (merchant-name *merchant*)
						  (transaction-key *transaction-key*))
  
  (let ((payment-profiles (if bill-to
			      (list 
				    :bill-to bill-to
				    :payment
				    (list :credit-card
					  (list :card-number card-number
						:expiration-date expiration-date)))
			      (list 			     
				    :payment
				    (list :credit-card
					  (list :card-number card-number
						:expiration-date expiration-date))))))
    (api-json "updateCustomerPaymentProfileRequest"
	      :merchant-name merchant-name
	      :transaction-key transaction-key
	      :parameter-plist
	      (list :customer-profile-id customer-profile-id     
		    :payment-profiles payment-profiles
		    :default-payment-profile (if default-payment-profile-p
						 "true"
						 "false")
		    :customer-payment-profile-id customer-payment-profile-id
		    :validation-mode "testMode"))))

(defun delete-customer-payment-profile-request (customer-profile-id customer-payment-profile-id
					     &key 
					       (merchant-name *merchant*)
					       (transaction-key *transaction-key*))
  (api-json "deleteCustomerPaymentProfileRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :customer-profile-id customer-profile-id
		  :customer-payment-profile-id customer-payment-profile-id)))
