(in-package :cl-auth-net)

(defun authentication-test-request (&key (merchant-name *merchant*)
				      (transaction-key *transaction-key*))
  (api-json "authenticateTestRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key))



(defun create-subscription-and-first-charge (merchant-customer-id
					     description
					     email
					     card-number
					     expiration-date
					     amount
					     subscription-start-date
					     &key (customer-type "individual"))
  
  (let ((customer-profile (api-call (create-customer-profile-request
				     merchant-customer-id
				     description
				     email
				     card-number
				     expiration-date
				     :customer-type customer-type))))

    (unless (success-p customer-profile)
      (error customer-profile))

    (when (success-p customer-profile)
      (let ((customer-profile-id (getf customer-profile :customer-profile-id) )
	    (customer-payment-profile-id (first
					  (getf customer-profile :customer-payment-profile-id-list)))
	    (start-date subscription-start-date ;;have to add a month to start date.
	     ))

	(unless (and customer-profile-id customer-payment-profile-id)
	  (error customer-profile))
		     
	(when (and customer-profile-id customer-payment-profile-id)

	  (let ((first-charge (api-call
			       (charge-profile-request customer-profile-id
						       customer-payment-profile-id
						       amount))))

	    (unless (success-p first-charge)
	      (error first-charge))

	    
	    (when (success-p first-charge)
	      (let ((subscription (api-call
				   (create-subscription-from-profile-request
				    customer-profile-id
				    customer-payment-profile-id
				    amount
				    start-date))))
		(unless (success-p subscription)
		  (error subscription))

		(when (success-p subscription)
		  (return-from create-subscription-and-first-charge
		    (values customer-profile first-charge subscription)))))))))))


(defun get-settled-batch-list-request (start end
				       &key
				       (merchant-name *merchant*)
				      (transaction-key *transaction-key*))
  (api-json "getSettledBatchListRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :first-settlement-date (if start
					     start
					     "2019-10-01T16:00:00Z")
		  :last-settlement-date (if end
					    end
					    "2019-10-31T16:00:00Z"))))

(defun get-transaction-list-request (batch-id
				     &key
				       (merchant-name *merchant*)
				       (transaction-key *transaction-key*))
  (api-json "getTransactionListRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :batch-id batch-id)))


(defun get-transaction-details-request (transaction-id
				     &key
				       (merchant-name *merchant*)
				       (transaction-key *transaction-key*))
  (api-json "getTransactionDetailsRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :trans-id transaction-id)))



