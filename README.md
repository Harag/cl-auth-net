# cl-auth-net

Implements a select few of authorize.net's api's. Any api calls not implemented can be executed using api-call directly.

- Code is in pre-alpha state so use at own peril.

Merchant information is captured in *merchant* and *transaction-key*, or can be passed to *-request calls.

toggle-sandbox-mode will toggle between "https://apitest.authorize.net/xml/v1/request.api" and "https://api.authorize.net/xml/v1/request.api", effectively switching between sandbox and live mode.

To check which mode you are in use *sandbox-mode*. nil means you are in live mode and t that you are in sandbox mode.

The API calls are captured in simple to use functions ending with -request that returns a plist that can be altered, if required, before passing the request plist on to api-call to do the actual call to the api.

You can use error-p, ok-p and success-p to test for the success of the api call.

Use message-code-p to test for a specific message code.

Example:

(api-call 
 (charge-credit-card-request
  (random 99999)
  "370000000000002"
  "2019-12"
  "900"
  70.0))


Result Example:

(:TRANSACTION-RESPONSE
 (:RESPONSE-CODE "1" :AUTH-CODE "LC6M5E" :AVS-RESULT-CODE "Y" :CVV-RESULT-CODE
  "M" :CAVV-RESULT-CODE "2" :TRANS-ID "40041755399" :REF-TRANS-ID ""
  :TRANS-HASH "" :TEST-REQUEST "0" :ACCOUNT-NUMBER "XXXX0002" :ACCOUNT-TYPE
  "AmericanExpress" :MESSAGES
  ((:CODE "1" :DESCRIPTION "This transaction has been approved."))
  :TRANS-HASH-SHA2 "" :SUPPLEMENTAL-DATA-QUALIFICATION-INDICATOR 0)
 :REF-ID "86323" :MESSAGES
 (:RESULT-CODE "Ok" :MESSAGE ((:CODE "I00001" :TEXT "Successful."))))