(in-package :cl-auth-net)

(defun create-subscription-request (email customer-id 
				    card-number expiration-date
				    card-code amount				    
				    subscription-start-date
				    &key
				      (ref-id (random 99999))
				      (customer-type "individual")				      
				      (unit "months")
				      (subscription-length "12")
				      (subscription-occurences "9999")
                                      
				      (merchant-name *merchant*)
				      (transaction-key *transaction-key*))
  
  (api-json "ARBCreateSubscriptionRequest"
	      :merchant-name merchant-name
	      :transaction-key transaction-key
	      :parameter-plist
	      (list
	       :ref-id ref-id
	       :subscription (list
			      :payment
			      (list :credit-card
				    (list :card-number card-number
					  :expiration-date expiration-date
					  :card-code card-code))
			      
			      :customer (list :type customer-type
					      :id customer-id
					      :email email)
			      :payment-schedule (list :interval
						      (list 
						       :length subscription-length
						       :unit unit)
						      :start-date subscription-start-date
						      :total subscription-occurences)
			      :amount amount))))


(defun create-subscription-from-profile-request (customer-profile-id
						 customer-payment-profile-id
						 amount
						 subscription-start-date
						 &key
						   (ref-id (random 99999))
						   (unit "months")
						   (subscription-length "12")
						   (subscription-occurences "9999")
						   
						   (merchant-name *merchant*)
						   (transaction-key *transaction-key*))
  
  (api-json "ARBCreateSubscriptionRequest"
	      :merchant-name merchant-name
	      :transaction-key transaction-key
	      :parameter-plist
	      (list
	       :ref-id ref-id
	       :subscription
	       (list
		:payment-schedule (list :interval
					(list 
					 :length subscription-length
					 :unit unit)
					:start-date subscription-start-date
					:total-occurrences subscription-occurences)
		:amount amount
		:profile (list :customer-profile-id customer-profile-id
			       :customer-payment-profile-id customer-payment-profile-id
			       )))))

(defun get-subscription-request (subscription-id
				 &key
				   (ref-id (random 99999))
				   (include-transactions "True")
				   (merchant-name *merchant*)
				   (transaction-key *transaction-key*))
  (api-json "ARBGetSubscriptionRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :ref-id ref-id
		  :subscription-id subscription-id
		  :include-transactions include-transactions)))


(defun get-subscription-status-request (subscription-id
					&key
					  (ref-id (random 99999))
					  (merchant-name *merchant*)
					  (transaction-key *transaction-key*))
  (api-json "ARBGetSubscriptionStatusRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :ref-id ref-id
		  :subscription-id subscription-id)))

(defun update-subscription-request (subscription-id email customer-id 
				    card-number expiration-date
				    card-code amount				    
				    subscription-start-date
				    &key
				      (ref-id (random 99999))
				      (customer-type "individual")				      
				      (unit "months")
				      (subscription-length "12")
				      (subscription-occurences "9999")
                                      
				      (merchant-name *merchant*)
				      (transaction-key *transaction-key*))
  
  (api-json "ARBUpdateSubscriptionRequest"
	      :merchant-name merchant-name
	      :transaction-key transaction-key
	      :parameter-plist
	      (list
	       :ref-id ref-id
	       :subscription-id subscription-id
	       :subscription (list
			      :payment
			      (list :credit-card
				    (list :card-number card-number
					  :expiration-date expiration-date
					  :card-code card-code))
			      
			      :customer (list :type customer-type
					      :id customer-id
					      :email email)
			      :payment-schedule (list :interval
						      (list 
						       :length subscription-length
						       :unit unit)
						      :start-date subscription-start-date
						      :total subscription-occurences)
			      :amount amount))))

(defun cancel-subscription-request (subscription-id
					&key
					  (ref-id (random 99999))
					  (merchant-name *merchant*)
					  (transaction-key *transaction-key*))
  (api-json "ARBCancelSubscriptionRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist
	    (list :ref-id ref-id
		  :subscription-id subscription-id)))


(defun get-customer-payment-profile-list-request (&key
						    (paging-limit "1000")
						    (paging-offset "1")
						    (search-type "subscriptionActive")
						    (merchant-name *merchant*)
						    (transaction-key *transaction-key*))
  (api-json "ARBGetSubscriptionListRequest"
	    :merchant-name merchant-name
	    :transaction-key transaction-key
	    :parameter-plist	    
	    (list :search-type search-type                  
		  :sorting (list :order-by "id"
				 :order-descending "false")
		  :paging (list :limit paging-limit
				:offset paging-offset)
	     )))
