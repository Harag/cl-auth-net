(in-package :cl-auth-net)

(defun charge-credit-card-request (ref-id card-number expiration-date card-code amount
				   &key
				     customer-id
				     (transaction-type "authCaptureTransaction")
				     (merchant-name *merchant*)
				     (transaction-key *transaction-key*))
  
  (let* ((transaction-plist
	  (list :transaction-type transaction-type
		:amount amount
		:payment (list :credit-card
			       (list :card-number card-number
				     :expiration-date expiration-date
				     :card-code card-code))))
	 (plist (list :ref-id ref-id
		      :transaction-request
		      (if customer-id
			  (append transaction-plist
				  (list :customer-id customer-id))
			  transaction-plist))))

    (api-json "createTransactionRequest" :merchant-name merchant-name
	      :transaction-key transaction-key
	      :parameter-plist plist)))


(defun charge-bank-card-request (ref-id account-type account-number routing-number
				 name-on-account amount
				 &key
				   customer-id
				   (transaction-type "authCaptureTransaction")
				   (merchant-name *merchant*)
				   (transaction-key *transaction-key*))
  
  (let* ((transaction-plist
	  (list :transaction-type transaction-type
		:amount amount
		:payment (list :bank-account
			       (list :account-type account-type
				     :account-number account-number
				     :routing-number routing-number
				     :name-on-account name-on-account))))
	 (plist (list :ref-id ref-id
		      :transaction-request
		      (if customer-id
			  (append transaction-plist
				  (list :customer-id customer-id))
			  transaction-plist))))

    (api-json "createTransactionRequest" :merchant-name merchant-name
	      :transaction-key transaction-key
	      :parameter-plist plist)))

(defun charge-profile-request (customer-profile-id payment-profile-id amount
			       &key
				 (ref-id (random 9999))
				 (transaction-type "authCaptureTransaction")
				 (merchant-name *merchant*)
				 (transaction-key *transaction-key*))
  
  (let* ((transaction-plist
	  (list :transaction-type transaction-type
		:amount amount
		:profile (list :customer-profile-id customer-profile-id
			       :payment-profile
			       (list :payment-profile-id payment-profile-id))))
	 (plist (list :ref-id ref-id
		      :transaction-request transaction-plist)))

    (api-json "createTransactionRequest" :merchant-name merchant-name
	      :transaction-key transaction-key
	      :parameter-plist plist)))

